//
//  DetailRowController.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-16.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation
import WatchKit

class DetailRowController: NSObject {
    let dateFormatter = DateFormatter()
    
    @IBOutlet var lblDeviation: WKInterfaceLabel!
    @IBOutlet var lblExpected: WKInterfaceLabel!
    
    func load(with model: DepartureModel) {
        dateFormatter.dateFormat = "HH:mm"
    
        if let expected = model.expected {
            lblExpected.setText(dateFormatter.string(from: expected))
        } else {
            lblExpected.setText(model.arrivesIn)
        }
        
        if model.deviation > 0 {
            lblDeviation.setText("\(String(model.deviation))m")
        }
    }
}

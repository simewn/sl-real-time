//
//  WatchSessionManager.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-15.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation
import WatchConnectivity
let settingsUpdatedName = Notification.Name.init("SettingsUpdated")

class WatchSessionManager: NSObject, WCSessionDelegate {
    private let session = WCSession.default()
    public
    
    func startSession() {
        session.delegate = self
        session.activate()
    }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        UserDefaults.standard.set(userInfo["siteId"], forKey: "siteId")
        UserDefaults.standard.set(userInfo["siteName"], forKey: "siteName")
        NotificationCenter.default.post(Notification(name: settingsUpdatedName))
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
}

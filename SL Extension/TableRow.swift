//
//  TableRow.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-11.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import WatchKit

class TableRow: NSObject {
    @IBOutlet var lbl: WKInterfaceLabel!
    
    var text: String? {
        didSet {
            lbl.setText(text)
        }
    }
}

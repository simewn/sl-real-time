//
//  InterfaceController.swift
//  SL Extension
//
//  Created by Simon Nyberg on 2016-11-08.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController  {
    let repo = DepartureRepository()
    var data = [String: [DepartureModel]]() {
        didSet {
            dataKeys = data.keys.sorted()
        }
    }
    var dataKeys = [String]()
    
    @IBOutlet var table: WKInterfaceTable!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(InterfaceController.refreshData), name: settingsUpdatedName, object: nil)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        refreshData()
    }
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        let row = table.rowController(at: rowIndex) as! TableRow
        let station = row.text!
        let departures = data[station]
        
        return departures
        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func refreshData() {
        if let siteName = UserDefaults.standard.string(forKey: "siteName") {
            self.setTitle(siteName)
        }
        
        if let siteId = UserDefaults.standard.string(forKey: "siteId") {
            repo.refresh(with: siteId, completion: { (error: String?, resp: [String: [DepartureModel]]) in
                self.data = resp
                self.load()
            })
        }
    }
    
    private func load() {
        table.setNumberOfRows(data.count, withRowType: "TableRow")
        
        for (index, key) in dataKeys.enumerated() {
            let row = table.rowController(at: index) as! TableRow
            row.text = key
        }
    }

}

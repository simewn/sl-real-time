//
//  DetailInterfaceController.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-15.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import WatchKit
import Foundation


class DetailInterfaceController: WKInterfaceController {
    @IBOutlet var table: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        let data = context as! [DepartureModel]
        
        let anyDeparture = data[0]
        self.setTitle(anyDeparture.destination)
        
        self.load(with: context as! [DepartureModel])
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func load(with departures: [DepartureModel]) {
        table.setNumberOfRows(departures.count, withRowType: "TableRow")
        
        for (index, departure) in departures.enumerated() {
            let row = table.rowController(at: index) as! DetailRowController
            row.load(with: departure)
        }
    }

}

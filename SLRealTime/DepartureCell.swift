//
//  DepartureCell.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-10-05.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation
import UIKit

class DepartureCell : UITableViewCell {
    fileprivate let deviationLabel = UILabel()
    fileprivate let estimatedLabel = UILabel()
    fileprivate let scheduledLabel = UILabel()
    fileprivate let dateFormatter = DateFormatter()
    
    var model: DepartureModel? {
        didSet {
            dateFormatter.dateFormat = "HH:mm"
            
            if let expected = model!.expected,
                let scheduled = model!.scheduled
            {
                estimatedLabel.text = dateFormatter.string(from: expected)
                deviationLabel.text = "\(String(model!.deviation)) min"
                scheduledLabel.text = dateFormatter.string(from: scheduled)
            
            }
            if let arrivesIn = model!.arrivesIn {
                scheduledLabel.text = arrivesIn
            }
            estimatedLabel.isHidden = model!.deviation == 0
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "Cell")
    
        self.translatesAutoresizingMaskIntoConstraints = false
        
        //estimatedLabel.backgroundColor = UIColor.green
        //scheduledLabel.backgroundColor = UIColor.red
        
        deviationLabel.translatesAutoresizingMaskIntoConstraints = false
        estimatedLabel.translatesAutoresizingMaskIntoConstraints = false
        scheduledLabel.translatesAutoresizingMaskIntoConstraints = false
        
        estimatedLabel.textColor = UIColor.red
        
        self.contentView.addSubview(scheduledLabel)
        self.contentView.addSubview(estimatedLabel)
        self.contentView.addSubview(deviationLabel)
        
        scheduledLabel.leadingAnchor.constraint(equalTo: super.contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        scheduledLabel.trailingAnchor.constraint(equalTo: estimatedLabel.leadingAnchor, constant: -10).isActive = true
        scheduledLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
        scheduledLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        
        estimatedLabel.widthAnchor.constraint(equalTo: scheduledLabel.widthAnchor).isActive = true
        estimatedLabel.centerYAnchor.constraint(equalTo: scheduledLabel.centerYAnchor).isActive = true
        
        deviationLabel.widthAnchor.constraint(equalTo: super.contentView.widthAnchor, multiplier: 0.20).isActive = true
        deviationLabel.trailingAnchor.constraint(equalTo: super.contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        deviationLabel.centerYAnchor.constraint(equalTo: scheduledLabel.centerYAnchor).isActive = true
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
}

//
//  SiteModel.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-01.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation

struct SiteModel {
    let siteId, name: String
}

extension SiteModel{
    init?(dict: [String: String]) {
        if let safeName = dict["Name"], let safeSiteId = dict["SiteId"] {
            self.name = safeName
            self.siteId = safeSiteId
        } else {
            return nil
        }
        
    }
}

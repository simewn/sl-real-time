//
//  WatchSync.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-11-15.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation
import WatchConnectivity

class WatchSync: NSObject, WCSessionDelegate {
    private let session: WCSession? = WCSession.isSupported() ? WCSession.default() : nil
    func start() {
        session?.delegate = self
        session?.activate()
    }
    func transferUserData() {
        if let validSession = session {
            if validSession.isPaired && validSession.isWatchAppInstalled {
                validSession.transferUserInfo(getUserData())
            }
        }
    }
    
    private func getUserData() -> [String: String] {
        let siteId = UserDefaults.standard.string(forKey: "siteId")!
        let siteName = UserDefaults.standard.string(forKey: "siteName")!
        
        return ["siteId": siteId, "siteName": siteName]
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}

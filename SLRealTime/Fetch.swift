//
//  fetch.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 09/09/16.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation
typealias requestCompletion = (Data?, URLResponse?, Error?)
struct Fetch {
    
    static let DEPARTURE_TYPES = ["Buses", "Trains", "Metros", "Ships", "Trams"]

    func getSitesStarting(with name: String, completion: @escaping (_ error: Error?, _ result: [SiteModel]) -> Void) -> URLSessionDataTask? {
        if name == "" {
            completion(nil, [])
            return nil
        }
        
        let escapedName = name.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        if let url = URL(string: "https://api.sl.se/api2/typeahead.json?key=861c46e52e9243bcb2722d64e62588a5&searchstring=\(escapedName)") {
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: getSitesCompletionHandler(callback: completion))
            
            task.resume()
            return task
        }
        
        return nil
    }
    
    private func getSitesCompletionHandler(
        callback: @escaping (_ error: Error?, _ result: [SiteModel]) -> Void)
        -> (requestCompletion)
        -> Void {
        return { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                if error != nil {
                    callback(error, [])
                    return
                }
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! [String: AnyObject]
                let sitesJson = json["ResponseData"]! as! [AnyObject]
                var res = [SiteModel]()
                for site in sitesJson {
                    res.append(SiteModel(dict: site as! [String: String])!)
                }
                
                callback(nil, res)
            } catch {
                print("SiteId Error", error)
                callback(error, [])
            }
        }
    }
    
    func getDeparturesFrom(
        _ siteId: String,
        completion: @escaping (_ error: String?, _ result: [String: [String: [DepartureModel]]]) -> Void)
        -> Void {
        let url = URL(string: "https://api.sl.se/api2/realtimedepartures.json?key=621c7fd5e76246fd89dc9f85edbc8699&siteid=\(siteId)&timewindow=30")
        
        let task = URLSession.shared.dataTask(with: url!, completionHandler: getDeparturesCompletionHandler(callback: completion))
        task.resume()
    }
    
    private func getDeparturesCompletionHandler(
        callback: @escaping (_ error: String?, _ result: [String: [String: [DepartureModel]]]) -> Void)
        -> (requestCompletion)
        -> Void {
        return { ( data: Data?, response: URLResponse?, error: Error? ) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! [String: AnyObject]
                let departureTypes = json["ResponseData"]! as! [String: AnyObject]
                
                callback(nil, self.mapResponseFromDict(departureTypes))
            } catch {
                print("Serialization error", error)
                callback(error.localizedDescription, [:])
            }
        }
    }
    
    private func mapResponseFromDict(_ departures: [String: AnyObject]) -> [String: [String: [DepartureModel]]] {
        var response = [String: [String: [DepartureModel]]]()
        
        for departureType in Fetch.DEPARTURE_TYPES {
            response[departureType] = [String: [DepartureModel]]()
            
            for departure in departures[departureType]! as! [AnyObject] {
                if let mappedDeparture = DepartureModel(dict: departure as! [String: AnyObject]) {
                    
                    var safeDepartures = response[departureType]![mappedDeparture.destination]
                    if safeDepartures == nil {
                       safeDepartures = []
                    }
                    
                    safeDepartures!.append(mappedDeparture)
                    
                    response[departureType]![mappedDeparture.destination] = safeDepartures
                }
            }
        }
        
        return response
    }
    
}

//
//  DepartureRepository.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-10-18.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation

class DepartureRepository {
    var departures = [String: [String: [DepartureModel]]]()
    
    let departureFetcher = Fetch()
    
    func refresh(with siteId: String, completion: @escaping (_ error: String?, _ result: [String: [DepartureModel]]) -> Void) {
        departureFetcher.getDeparturesFrom(siteId, completion: { (error, response) in
            self.departures = response
            completion(error, self.merge())
        })
    }
    
    func filter(with travelType: String) -> [String: [DepartureModel]] {
        if travelType == "All" {
            return merge()
        }
        
        if let filteredDepartures = departures[travelType] {
            return filteredDepartures
        }
        
        return [String: [DepartureModel]]()
    }
    
    func merge() -> [String: [DepartureModel]] {
        var result = [String: [DepartureModel]]()
        for departureType in self.departures.keys.sorted() {
            for destination in self.departures[departureType]!.keys.sorted() {
                result[destination] = self.departures[departureType]![destination]
            }
        }
        return result
    }
    
    func typesWithDepartures() -> [String] {
        var types = ["All"]
        
        for key in departures.keys {
            let stations = departures[key]!
            if (stations.filter{$1.count > 0}.count > 0) {
                types.append(key)
            }
        }
        
        return types
    }
}

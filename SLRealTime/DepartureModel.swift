//
//  DepartureModel.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 09/09/16.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import Foundation

struct DepartureModel {
    let deviation: Int
    let destination, line: String
    var expected, scheduled: Date?
    let arrivesIn: String?
}

extension DepartureModel{
    init?(dict: [String: AnyObject]) {
        if let lineNumber = dict["LineNumber"],
            let destination = dict["Destination"]
        {
            self.line = lineNumber as! String
            self.destination = destination as! String
        } else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        
        if dict["TimeTabledDateTime"] == nil || dict["ExpectedDateTime"] == nil {
            self.deviation = 0
            self.arrivesIn = dict["DisplayTime"] as? String
            expected = nil
            scheduled = nil
        } else {
            self.arrivesIn = nil
            let unsafeScheduled = dateFormatter.date(from: dict["TimeTabledDateTime"] as! String)
            let unsafeExpected = dateFormatter.date(from: dict["ExpectedDateTime"] as! String)
            
            let span: TimeInterval?
            
            if let safeExpected = unsafeExpected,
                let safeScheduled = unsafeScheduled
            {
                self.expected = safeExpected
                self.scheduled = safeScheduled
                span = self.expected!.timeIntervalSince(self.scheduled!)
            } else {
                span = nil
                self.expected = Date()
                self.scheduled = Date()
            }
            
            if let validSpan = span {
                self.deviation = Int(floor(validSpan/60))
            } else {
                self.deviation = 0
            }
        }
      
        
    }
}

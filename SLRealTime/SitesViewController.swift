//
//  SitesViewController.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-10-29.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import UIKit

class SitesViewController: UITableViewController, UISearchBarDelegate {
    private let fetch = Fetch()
    private var ds = [SiteModel]()
    private let cellIdentifier = "SiteIdentifier"
    private let searchController = UISearchController(searchResultsController: nil)
    private var dataTask: URLSessionDataTask?
    private let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private var searchIcon: UIView?
    private var searchField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        for view in searchController.searchBar.subviews[0].subviews {
            if view is UITextField {
                searchField = view as? UITextField
                break
            }
        }
        
        searchIcon = searchField?.leftView
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.tableHeaderView = searchController.searchBar
        title = "Set station"
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.perform(#selector(SitesViewController.showKeyboard), with: nil, afterDelay: 0.005)
    }
    
    func showKeyboard() {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SitesViewController.search), object: nil)
        self.perform(#selector(SitesViewController.search), with: nil, afterDelay: 0.5)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        stopAnimating()
    }
    
    func startAnimating() {
        searchField?.leftView = spinner
        spinner.startAnimating()
    }
    
    func stopAnimating() {
        searchField?.leftView = searchIcon
        spinner.performSelector(onMainThread: #selector(spinner.stopAnimating), with: nil, waitUntilDone: true)
    }
    
    func search() {
        let stop = searchController.searchBar.text!
        if let task = dataTask {
            task.cancel()
        }
        startAnimating()
        self.dataTask = fetch.getSitesStarting(with: stop, completion: { (err: Error?, result: [SiteModel]) in
            self.ds = result
            self.tableView.performSelector(onMainThread: #selector(self.tableView.reloadData), with: nil, waitUntilDone: true)
            self.stopAnimating()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ds.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let model = ds[indexPath.row]
        cell.textLabel?.text = model.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let siteModel = ds[indexPath.row]
        saveSiteId(fromSite: siteModel)
        let _ = self.navigationController?.popViewController(animated: true)
    }

    private func saveSiteId(fromSite: SiteModel) {
        UserDefaults.standard.set(fromSite.siteId, forKey: "siteId")
        UserDefaults.standard.set(fromSite.name, forKey	: "siteName")
    }

}

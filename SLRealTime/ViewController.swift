//
//  ViewController.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 08/08/16.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import UIKit
import OHHTTPStubs

class ViewController: UITableViewController {
    private let fetcher = Fetch()
    private let reuseIdentifier = "Reuse"
    private let repository = DepartureRepository()
    private let watchSync = WatchSync()

    private var dataset = [String: [DepartureModel]]() {
        didSet {
            self.sectionKeys = dataset.keys.sorted()
        }
    }
    
    private var sectionKeys = [String]()
    private let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    private let travelTypes = ["All", "Buses", "Metros", "Trains", "Ships","Trams"]
    private var travelTypesView: UISegmentedControl
    
    override init(style: UITableViewStyle) {
        travelTypesView = UISegmentedControl(items: travelTypes)
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(DepartureCell.self, forCellReuseIdentifier: reuseIdentifier)
        
        configureSpinner()
        configureTableView()
        configureTravelTypes()
        configureNavigationItemButtons()
        
        guard let _ = getStoredSiteId() else {
            return loadSitesViewController()
        }
    }
    
    private func enableSegmentsFor(availableTypes: [String]) {
        for (index,type) in travelTypes.enumerated() {
            DispatchQueue.main.async {
                self.travelTypesView.setEnabled(availableTypes.contains(type), forSegmentAt: index)
            }
        }
    }
    
    private func configureTravelTypes() {
        travelTypesView.selectedSegmentIndex = 0
        travelTypesView.addTarget(self, action: #selector(ViewController.travelTypesClick), for: UIControlEvents.valueChanged)
    }
    
    private func configureNavigationItemButtons() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Set station", style: .plain, target: self, action: #selector(ViewController.loadSitesViewController))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(ViewController.refreshDepartures))
    }
    
    private func configureTableView() {
        self.tableView.tableHeaderView = travelTypesView
        self.tableView.separatorColor = UIColor.clear
        self.tableView.allowsSelection = false
    }
    
    private func configureSpinner() {
        self.view.addSubview(spinner)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerYAnchor).isActive = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.watchSync.transferUserData()
        
        self.title = getStoredSiteName()
        refreshDepartures()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadSitesViewController() {
        let vc = SitesViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func travelTypesClick(segment: UISegmentedControl) {
        let travelType = segment.titleForSegment(at: segment.selectedSegmentIndex)
        self.dataset = repository.filter(with: travelType!)
        self.tableView.reloadData()
    }

    func refreshDepartures() {
        spinner.startAnimating()
        
        repository.refresh(with: getStoredSiteId()!, completion: { (error, response) in
            self.dataset = response
            
            self.spinner.performSelector(onMainThread: #selector(self.spinner.stopAnimating), with: nil, waitUntilDone: false)
            self.tableView.performSelector(onMainThread: #selector(self.tableView.reloadData), with: nil, waitUntilDone: true)
            self.travelTypesView.selectedSegmentIndex = 0
            
            self.enableSegmentsFor(availableTypes: self.repository.typesWithDepartures())
        })
    }

    func getStoredSiteId() -> String? {
        return UserDefaults.standard.string(forKey: "siteId")
    }
    
    func getStoredSiteName() -> String? {
        return UserDefaults.standard.string(forKey: "siteName")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! DepartureCell
        
        let key = sectionKeys[(indexPath as NSIndexPath).section]
        let departure = self.dataset[key]![(indexPath as NSIndexPath).row]
        cell.model = departure
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionKeys[section]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionKeys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = self.sectionKeys[section]
        
        return self.dataset[key]!.count
    }
}


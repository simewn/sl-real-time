//
//  FetchTest.swift
//  SLRealTime
//
//  Created by Simon Nyberg on 2016-10-15.
//  Copyright © 2016 Simon Nyberg. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import SLRealTime

class FetchTest: XCTestCase {
    var response = [String: [String: [DepartureModel]]]()
    var error: String?
    
    override func setUp() {
        super.setUp()
        let returnData = ["ResponseData": [ "Buses": [ [ "Destination": "Gullmarsplan", "LineNumber": "4", "TimeTabledDateTime": "2016-10-15T21:47:00", "ExpectedDateTime": "2016-10-15T21:46:00" ]], "Ships":[], "Trams":[], "Trains":[], "Metros":[]]]
        
        let fetch = Fetch()
        let expect = expectation(description: "lol")
        
        let _ = stub(condition: isHost("api.sl.se"), response: { _ in
            return OHHTTPStubsResponse(jsonObject: returnData, statusCode: 200, headers: nil)
        })
        
        fetch.getWithSiteId("1464", completion: { (error: String?, response: [String: [String: [DepartureModel]]]) -> Void in
            self.response = response
            self.error = error
            expect.fulfill()
        })

        self.waitForExpectations(timeout: 10, handler: { (error) in })
    }
    override func tearDown() {
        super.tearDown()
        
        response = [String: [String: [DepartureModel]]]()
        error = nil
    }
    
    func testToIncludeAllTravelTypes() {
        XCTAssertEqual(response.keys.count, 5)
    }
    
    func testThatBusesHasOneItem() {
        XCTAssertEqual(response["Buses"]?.keys.count, 1)
    }
    
    func testMappingToCorrectDestination() {
        let buses = response["Buses"]!["Gullmarsplan"]
        XCTAssertNotNil(buses)
        let bus = buses?.first
        XCTAssertNotNil(bus)
    }
}
